# ，file 文件

## 文件和目录

ls -l 中文件的类型含义：
- d，目录
- -，普通文件
- l，符号链接文件
- b, c，外围设备
- s, p，系统数据结构、管道

## 文件和目录的权限

用户
- u， 创建者
- g，同组用户
- o，other

### 权限描述
- r，对于文件是读取内容，对于目录是浏览
- w，对于文件是修改内容，对于目录是新增、删除、移动目录内文件
- x，对于文件时执行，对于目录是进入目录

### 权限掩码

umask 0027

- 文件权限是：666-027 = 640，文件默认没有执行权限
- 目录权限是：777-027 = 750


### 特殊权限

- s(S)，SUID，该文件所有者的全部资源
- s(S)，SGID，该文件所在组的全部资源，四位数字最前
- t(T)，Stricky，每位用户拥有完整权限操作目录

开启时小写，没开启是大写。

如/tmp，/var/tmp，每位用户可以完全控制自己的文件。

## 文件的时间

文件创建后，没修改，修改时间=创建时间；
没修改过状态，状态改变时间=创建时间；
没读取过，访问时间=创建时间。

- Access，访问时间，读取内容时，more，cat
- Modify，修改时间，vi，touch
- Change，状态改变时间，chmod

stat 文件名，查看时间

man -s 2 stat


ls -lctime
ls -lmtime
ls -t(默认是修改时间) 

- ls -lc file, ctime更改时间，st_ctime
- ls -lu file，atime访问时间，st_atime
- ls -l file，mtime修改时间,st_mtime


find -mtime -1 |xargs ls -l 



