# Graphic 图形

## Gralloc

Gralloc作为硬件抽象层，用于管理帧缓冲区；
FrameBuffer是Linux内核提示的显示抽象层；/dev/graphics/fb*；



## 渲染

- OpenGL ES：用于渲染2D和3D图形的跨平台API。该库可用于实现硬件加速的3D渲染。

AOSP使用OpenGL ES(GLES) API来渲染图性。为了创建GLES上下文并为GLES渲染提供窗口系统，AOSP使用EGL库。
GLES调用用于渲染纹理多边形，而GEL调用用于将渲染放到屏幕上。

- Vulkan：适用于高性能3D图形的低开销、跨平台API。

- HWC： Hardware Composer，硬件合成器


# Display 显示

## Surface：表示要合成到界面的内存块。





# Distirbuted Display


## 无线投屏
Wireless display technologies，无线显示技术

- AirPlay
  Apple开发的专有无线通信协议套件，允许在设备之间传输音视频、屏幕和照片，以及相关元数据。
  接收器：TV、HomePod、支持A2DP配置文件的蓝牙设备。

- Miracast
  由Wi-Fi联盟与2012年推出。描述为HDMI over Wi-Fi，取代线缆。基于对等Wi-Fi Direct标准。获得Miracast认证的设备可以相互通信。
  允许发送1080p的高清视频（H.264编码）和5.1环绕声（AAC和AC3编码）
  应用层，流是通过RTSP、RTP来做数据传输的。

- Google Cast
  Google开发的专有协议，在兼容的消费设备上播放互联网流式音视频内容。
  以Chromecast内置的形式销售。
  也可以通过Chrome浏览器中的Cast扩展，实现投屏。
  
- WHDI: Wireless Home Digital Interface
  5GHz,

- Wireless HD
  60GHz, 7GHz的频宽。
  
- WiGig: wireless Gigabit Alliance
  60GHz, 802.11ad协议。
  
- WiDi: Wireless Display
  60GHz, Intel开发。

- DLNA: Digital Life Network Alliance
  包含用于媒体管理和设备发现控制的通用即插即用，以及广泛使用的数字媒体格式以及有线和无线网络标准。
  
    角色：DMP是主动拉取，DMR是接收推过来的内容。
    + DMS: Digital Media Server，存储内容，并使其可供联网DMP和DMR使用。如PC和NAS设备。
    + DMP: Digital Media Player，在DMS上查找内容并提供播放和渲染功能。如但是，音响设备。
    + DMR: Digital Media Render，按照DMC的指示播放内容，从DMS中查找内容。如电视、音视频接收器、视频显示器；单个设备（如电视、AV接收器）可以同时用作DMR（从DMS接收推送内容）和DMP（从DMS拉取内容）。
    + DMC: Digital Media Controller, 在DMS上查找内容并指示DMR播放。 内容不会通过DMC传输。如手机和平板。
  
  DLNA isn’t really a wireless display solution. Instead, it’s simply a way to take content on one device and play it on another.
  
## 参考资料
- The Main Wireless HDMI Transmission Protocols and Their Typical Products
  https://www.portablehifi.com/wireless-hdmi-transmission-protocols-products