# driver framework

驱动框架，支持驱动硬件设备

## OS相关的驱动框架

- Linux driver，Linux驱动框架
  
  The Linux driver implementer’s API guide
  https://kernel.org/doc/html/latest/driver-api/index.html

- WDF, Windows Driver framework，驱动框架
  Windows 硬件开发人员文档
  https://docs.microsoft.com/zh-cn/windows-hardware/drivers

  Getting started with Windows drivers
  https://docs.microsoft.com/en-us/windows-hardware/drivers/gettingstarted/

- IO/Kit, iOS驱动框架
  Introduction to I/O Kit Fundamentals
  https://developer.apple.com/library/archive/documentation/DeviceDrivers/Conceptual/IOKitFundamentals/Introduction/Introduction.html


## 芯片相关

- CMSIS，Common Microcontroller Software Interface Standard，通用微处理器软件接口标准
  
  Common Microcontroller Software Interface Standard (CMSIS)
  https://developer.arm.com/tools-and-software/embedded/cmsis

  The CMSIS is a set of tools, APIs, frameworks, and work flows that help to simplify software re-use, reduce the learning curve for microcontroller developers, speed-up project build and debug, and thus reduce the time to market for new applications.
  https://arm-software.github.io/CMSIS_5/Core_A/html/index.html
  
  
  Keil, 微处理器工具集。 The Cortex Microcontroller Software Interface Standard (CMSIS) 
  https://www2.keil.com/mdk5/cmsis

