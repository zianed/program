# C++


## C++标准


C++标准第一版，1998年发布。正式名称为ISO/IEC 14882:1998。


- ISO/IEC 14882:2020
Programming languages — C++

https://www.iso.org/standard/79358.html


- C++ - Standards

https://www.open-std.org/jtc1/sc22/wg21/docs/standards


## __cplusplus

版本的年月表达

- 在C++03标准中，__cplusplus被定义为199711L
- 在C++11标准中，__cplusplus被定义为201103L
