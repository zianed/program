# Memory 内存相关



### Volatile Memory，易失性存储器
电流中断后，所存储的数据会消失。

- RAM，Random Access Memory，随机存取存储器

  - SRAM，Static，静态随机存取存储器
    - CPU Cache

  - DRAM，Dynamic，动态随机存取存储器
    - DDR，Double Data Rate Synchronous DRAM，双倍数据传输率的SDRAM



### Non-volatitle Memory，非易失性存取器
电流中断后，所存储的数据不会消失。重新供电后，可以读取数据。

- ROM, Read Only Memory，只读存储器

+ EPROM 可擦除可编程只读存储器，Erasable Programmable Read-Only Memory。EPROM是一种具有可擦除功能，擦除后即可进行再编程的ROM内存
+ EEPROM 擦出快，Electrically Erasable Programmable read only memory。一种掉电后数据不丢失的存储芯片。

- Flash memory，闪存

- 计算机存储设备（硬盘、磁盘、磁带）





## 标准组织

- JEDEC
JEDEC固态技术协会



