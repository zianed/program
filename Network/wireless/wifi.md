# Wi-Fi相关

Wi-Fi is a family of wireless network protocols, based on the IEEE 802.11 family of standards, which are commonly used for local area networking of devices and Internet access.

## Wi-Fi联盟
Wireless Fidelity Alliance
Wi-Fi 是非盈利组织Wi-FI联盟的注册商标，保证各产品之间是兼容的，可互操作。

Wi-Fi认证需要符合802.11无线电标准，WPA、WPA2安全标准以及EAP身份验证标准。


## Wi-Fi基础知识

Wi-Fi可以实现：
- 无线数据传输，
- Wi-Fi定位，使用Wi-Fi热点识别设备位置
- Wi-Fi传感，运动检测、手势识别


### 信号调制方式

载波，carrier ware，被调制以传播信号的波形，携带信息的波形。 基带(baseband) + 载波(carrier wave) = 宽带(broadband)。


将数字信号转成电磁波：
- DSSS，Direct Sequence Spread Spectrum，直接序列扩频
  抗干扰能力比较强，要求可视，两点间的路由上没有任何阻挡。

  在发端直接用具有高码率的扩频码序列对信息比特流进行调制, 从而扩展信号的频谱, 在接收端用与发送端相同的扩频码序列进行相关解扩, 把展宽的扩频信号恢复成原始信息。一种直接序列扩频技术是使用异或运算将数字信息流与扩展码位流结合起来.

  例如说在发射端将"1"用11000100110，而将"0"用00110010110去代替，这个过程就实现了扩频，而在接收机处只要把收到的序列是11000100110就恢复成"1"是00110010110就恢复成"0"，这就是解扩。这样信源速率就被提高了11倍，同时也使处理增益达到10dB以上，从而有效地提高了整机倍噪比。

  在发射机端，要传送的信息先转换成二进制数据或符号，先经过M-PSK调制后与伪随机码（PN码）进行相乘运算后形成复合码，然后送入信道加入高斯白噪 声。

  采用BPSK和DQPSK调制技术。

- FHSS，Frequency-Hopping Spread Spectrum，跳频技术
  用一定码序列进行选择的多频率频移键控。跳频扩频就是用扩频的码序列去进行移频键控(FSK)调制，使载波的频率不断地跳变。
  遵守FCC的要求，使用75个以上的跳频讯号、且跳频至下一个频率的最大时间间隔(Dwell Time)为400ms。

  采用GFSK调制技术

- OFDM，Orthogonal Frequency Division Multiplexing，正交频分复用
  之间干扰更大，在非视距的情况下可以进行数据传输。

  采用正交频分复用的调制方式，

  实际上OFDM是MCM(Multi Carrier Modulation)，多载波调制的一种。通过频分复用实现高速串行数据的并行传输, 它具有较好的抗多径衰落的能力，能够支持多用户接入。
  OFDM技术是多载波传输方案的实现方式之一，它的调制和解调是分别基于IFFT和FFT来实现的，是实现复杂度最低、应用最广的一种多载波传输方案。


- OFDMA，Orthogonal Frequency Division Multiple Access，正交频分多址 

  OFDMA是OFDM技术的演进，将OFDM和FDMA技术结合。在利用OFDM对信道进行副载波化后，在部分子载波上加载传输数据的传输技术。

  OFDM是一种调制方式；OFDMA是一种多址接入技术，用户通过OFDMA共享频带资源，接入系统。



QAM，Quadrature Amplitude Modulation，正交幅度调制
在两个正交载波上进行幅度调制，正交载波

对于Wi-Fi，802.2制定的逻辑链路控制（LLC）采用Wi-Fi的媒体访问控制（MAC）来管理充实，而不依赖与更高级别的协议栈。

Wi-Fi P2P，Wi-Fi Direct实现ad-hoc传输，于2010年10月推出。


### 同频技术
指2.4G和5G两种频率上是否同时工作。

- SBSC，Single Band Single Concurrent，单频单发
  1个完整的基带处理 + 1个RF前端

- DBSC，Dual Band Single Concurrent，双频单发
  虚拟同步双频，VSDB，Virtual Simultaneous Dual Band
  2个完整的基带处理 + 1个RF前端
  其RF前端只能稳定选择一个频段进行工作

- DBDC，Dual Band Dual Concurrent，双频双发
  2个完整的基带处理 + 2个RF前端，两套独立通路

  - SDB，Simultaneous Dual Band，并发双频
    两块IC分别集成完整的通路做DBDC，路由器可以同时产生两个独立的无线网络分别对应2.4G和5G。

  - RSDB，Real Simultaneous Dual Band，同步双频
    一块IC内部集成2个完整的通路，即时同步双频。终端设备上实现并发一般采用该种模式。
    结合Multi-Path TCP实现并发链路上的数据传输。

- DBAC，Dual Band Adaptive Concurrent，双频自适应并发
  频段自适应连接技术。


终端通常是DBSC（单反），路由器通常是DBDC（双发）。


设备射频发射通路，
- a)基带处理（主要是PHY+MAC处理部分）
- b)RF前端（无线、PA、LNA），只能选在一个频段工作。

PA是Power Amplifier的简称，中文名称为功率放大器，简称“功放”，功率放大器，用于射频发射电路。
指在给定失真率条件下，能产生最大功率输出以驱动某一负载的放大器。
PA可以带来发射信号的放大。

LNA(Low Noise Amplifier)，低噪声放大器，用于射频接收电路。
LNA可以带来接收灵敏度的提升，即接收能力的提高。





## Wi-Fi信道

Wi-Fi使用2.4GHz、5GHz、6GHz、60GHz等频段，频段被细分为多个信道。信道可以在多个网络之间共享，但在任何时候只会有一个发送器在该信道上进行传输。

通常信道在一个频段内使用5MHZ间隔编号，60GHz的时候使用2.16GHz分割。
发射器通常占用20MHz，并且标准允许将信道绑定在一起形成更宽的通道已实现更高吞吐量。
更宽的信道，20MHz -> 160MHz，传输更多的数据。

电磁波频率越高，越接近光的属性，直线传播，可承载的数据量越大。
同一个频段内，频宽变大时，允许接入的设备就会变少，因而就需要更高的频段。

### List of WLAN channel

#### 2.4GHz信道：

| 信道 | 下限频率(MHz) | 中间频率(MHz) | 上限频率(MHz) |
|----|----|----|----|
|1  |2401 |2412 |2423 |
|6  |2426 |2437 |2448 |
|11 |2451 |2462 |2473 |
|13 |2406 |2472 |2483 |
|14 |2473 |2484 |2495 |

无干扰信道
- 频宽22MHz，1,6,11信道无干扰，
- 频宽20MHz，1,5,9,13信道无干扰
- 频宽40MHz，1 ~ 5, 9 ~ 13信道无干扰


#### 5GHz信道：

美国U-NII频段，Unlicensed National Information Infrastructure电磁波频段

- U-NII 1~4用于5GHz
- U-NII 5~8用于6GHz

5GHz信道：

| 信道 | 下限频率(MHz) | 中间频率(MHz) | 上限频率(MHz) |
|----|----|----|----|
|7    |5030 |5035 |5040 |
|196  |5970 |5980 |5990 |

支持20、40、80、160MHz频宽。

#### 6GHz信道：

| 信道 | 下限频率(MHz) | 中间频率(MHz) | 上限频率(MHz) |
|----|----|----|----|
|1    |5945 |5955 |5965 |
|221  |7045 |7055 |7065 |

支持20、40、80、160MHz频宽。


#### 60GHz信道：

| 信道 | 下限频率(GHz) | 中间频率(GHz) | 上限频率(GHz) |
|----|----|----|----|
|1    |57.24 |58.32 |59.40 |
|2    |59.40 |60.48 |61.56 |
|6    |68.04 |69.12 |70.20 |
|9    |57.24 |59.40 |61.56 |
|13   |65.88 |68.04 |70.20 |
|17   |57.24 |60.48 |63.72 |
|20   |63.72 |66.96 |70.20 |
|25   |57.24 |61.56 |65.88 |
|27   |61.56 |65.88 |70.20 |

信道带宽说明：
- 1~6信道，带宽2.16GHz
- 9~13信道，带宽4.32GHz
- 17~20信道，带宽6.48GHz
- 25~27信道，带宽8.64GHz

https://www.rfwireless-world.com/Terminology/WLAN-802-11-channels.html



## Wi-Fi性能
Wi-Fi的工作范围取决于频带、发射机的功率、接收器敏感度、天线增益、天线类型、调制技术、信号传播等。在更长的距离、更大的信号吸收下，速率通常也会降低。

Wi-Fi4及以上允许设备使在发射器和接收器上具有多个天线，多个天线使设备能够利用相同频带上的多径传播，从而提供更快的速度和更大的范围。

使用Wi-Fi时，视线通常效果最好，信号可以传输、吸收、反射、折射、衍射和上下淡入淡出，信号也会受到金属结构和谁的强烈影响。

Wi-Fi采用带有冲突避免的载波侦听多路访问CSMA/CA，还是会受到隐藏节点和暴露节点问题影响。



## 不同版本协议的速率

| Wi-Fi版本 | 802.11标准 | 频率 | 通道宽度 | MIMO | 最大数据速率 | 采纳年份 | 备注信息 |
|----|----|----|----|----|----|----|----|
|Wi-Fi 6e|802.11ax       |6 GHz     |20、40、80、160 MHz |多用户(MU-MIMO) |600 to 9608 Mbps |2020年 |最高8条空间流，最大副载波调制1024-QAM，半双工9.6Gbits |
|Wi-Fi 6 |802.11ax       |2.4/5 GHz |20、40、80、160 MHz |多用户(MU-MIMO) |600 to 9608 Mbps |2019年 |最高8条空间流，最大副载波调制1024-QAM，半双工9.6Gbits |
|Wi-Fi 5 |802.11ac wave2 |5 GHz     |20、40、80、160 MHz |多用户(MU-MIMO) |433 to 6933 Mbps |2014年 |最高8条空间流，最大副载波调制256-QAM，半双工6.9Gbits |
|Wi-Fi 5 |802.11ac wave1 |5 GHz     |20、40、80 MHz      |单用户(SU-MIMO) |433 to 6933 Mbps |2014年 |最高8条空间流，最大副载波调制256-QAM，半双工6.9Gbits |
|Wi-Fi 4 |802.11n        |2.4/5 GHz |20、40 MHz          |单用户(SU-MIMO) |72 to 600 Mbps |2008年 |最高4条空间流，最大副载波调制64-QAM，半双工600Mbits |
|Wi-Fi 3 |802.11g        |2.4 GHz   |20 MHz              |不适用          |6 to 54 Mbps |2003年 |
|Wi-Fi 2 |802.11a        |5 GHz     |20 MHz              |不适用          |6 to 54 Mbps |1999年 |
|Wi-Fi 1 |802.11b        |2.4 GHz   |20 MHz              |不适用          |1 to 11 Mbps |1999年 |
|Wi-Fi 0 |802.11         |2.4 GHz   |20 MHz              |不适用          |1 to 2 Mbps  |1997年 |


## 参考资料
- 不同的 Wi-Fi 协议和数据速率
https://www.intel.cn/content/www/cn/zh/support/articles/000005725/wireless/legacy-intel-wireless-products.html

