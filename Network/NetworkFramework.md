# Network Framework


### iOS Wi-Fi API overview

Explore the various Wi-Fi APIs available on iOS and their expected use cases.

https://developer.apple.com/documentation/technotes/tn3111-ios-wifi-api-overview

### Network

Create network connections to send and receive data using transport and security protocols.

https://developer.apple.com/documentation/network

### Multipeer Connectivity

Support peer-to-peer connectivity and the discovery of nearby devices.

https://developer.apple.com/documentation/multipeerconnectivity

