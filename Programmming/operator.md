# operator 运算符

## 不可重载的运算符

- 类成员 .
- 成员指针 .*
- 作用域 ::
- sizeof sizeof
- 三目运算符  ?:


## <<运算符重载

.h 中
```
friend ostream& operator<< (ostream &os, const Book &obj);
```

.cpp 中
```
ostream& operator<< (ostream &os, const Book &obj)
{
    return os;
}
```


## ++运算符

前缀
```
A A::opertor++(int)
{
    return *this;
}
```

后缀
```
A A::opertor++(int)
{
    A tmp(*this);
    ++ *this;
    return tmp;
}
```
