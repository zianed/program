# 字节序

一个字节按照8位（bit）表示。多字节Byte在内存中的表示，代表Byte在内存中存放的顺序。

+ MSB： Most Sinificant Bit，最高有效位，二进制中代表最高值的比特位。二进制表示最左边的位，对数值影响最大。
+ LSB： Least Sinificant Bit，最低有效位，二进制中代表最低值的比特位。二进制表示最右边的位，对数值影响最小。


## 字节序存放

内存地址：代表一个内存空间，一个byte的存储空间。

多个字节的数据在内存中是如何让存储的，每一个byte是按照内存的存储地址以高到低排序，还是从低到高排序。

### Big-Endian

大端模式：数据的高位放到内存的低地址端，低位放到内存的高地址端。更符合人类的思维方式，由低到高存数据。

如果地址从低往高排，大端模式与16进制表示相同；

### Samll-Endian

小端模式：数据的高位放到内存的低地址端，低位放到内存的高地址端。

如果地址从低往高排，大端模式与16进制表示每byte反着放；

### 数值表示实例

如一个占用2个字节的short类型数据0x1234，在内存中的存储方式。

|  |  |  |  |
| ------- | ------- | ------- | ------- |
| 大端模式 | 低地址 | 12 | 34 | 高地址 |
| 小端模式 | 低地址 | 34 | 12 | 高地址 |


如一个占用四个字节的int类型数据0x12345678，在内存中的存储方式。

|  |  |  |  |  |  |
| ------- | ------- | ------- | ------- | ------- | ------- |
| 大端模式 | 低地址 | 12 | 34 | 56 | 78 | 高地址 |
| 小端模式 | 低地址 | 78 | 56 | 34 | 12 | 高地址 |


## 计算机字节序

数据在存储器中的存储格式，不同的计算机处理器使用不同的字节序表示。

Intel的x86处理器使用小端字节序；PowerPC狐狸器使用大端字节序；Arm处理器既可以配置为大端，也可以配置为小端。

判断字节序：赋值int，取第一个byte进行比较，则可以判断是大端模式，还是小端模式。
```
#include <stdio.h>

int main(int argc, char **argv)
{
    int a = 0x12345678;
    char b = *((char *)(&a));
    if (b == 0x12) {
        printf("is big endian\n");
    } else {
        printf("is small endian\n");
    }
    return 0;
}
```


## 网络字节序

标准的网络字节序是BigEndian。

TCP/IP协议规定：把接收到的第一个字节当做高位字节对待。这就要求发送数据时，发送的第一个字节是该数值在内存中的起始地址处对应的那个字节。

Integers within network headers must be sent with the bytes in decreasing numerical significance, most-significant byte first. (The most-significant byte is nearest the start of the packet.)
This means that bytes or octets are transmitted in “raster-scan” order,left to right, top to bottom, as we look at the diagrams.
Bits within a byte are sent least-significant-bit first.

puts the group address indicator (i.e., the bit that defines whether an address is unicast or multicast) in the least significant bit of the first byte.
   
https://www.rfc-editor.org/rfc/rfc2469


### 网络字节序转换

计算机上的数据在跨网络传输时，一定要进行网络字节序转换处理，否则发端和收端字节序不一致时，会出现数据不匹配情况。

htons/ntohs/htonl/ntohl host字节序与network字节序转换。


# 参考资料
- 清晰讲解LSB、MSB和大小端模式及网络字节序
https://developer.aliyun.com/article/1206254

- A Caution On The Canonical Ordering Of Link-Layer Addresses
https://www.rfc-editor.org/rfc/rfc2469
