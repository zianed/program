# CAP

- C: Consistency, 一致性，所有节点一份副本，各个节点数据一致。
- A: Availability, 可用性， 数据更新具备高可用性；尽量返回数据，不会不响应，但不保证每个节点返回的数据都是最新的。强调服务可用，但是不保证数据的绝对一致。
- P: Partition Tolerance, 分区容错性，能容忍网络分区；强调的是集群对分区故障的容错能力。

P是必须要保证的，C和A可以选择。



 