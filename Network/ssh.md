# SSH


## 公钥修改

1、改/etc/sshd_config

- RSAAuthentication yes 启用RSA认证
- PubkeyAuthentication yes 启用公钥认证
- PasswordAuthentication no 禁止密码认证

2、重启

/etc/init.d/ssh restart

3、产生公钥

ssh-keygen -t rsa

在~/.ssh目录下
id_rsa为客户端密钥
id_rsa.pub为客户端公钥

4、配置公钥到服务器上

cat id_rsa.pub >> .ssh/authoried_keys

