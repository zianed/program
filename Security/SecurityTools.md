# 安全工具

## 商用工具

### Nessus

系统漏洞扫描

Vulnerability Assessment

### Center for Internet Security

https://www.cisecurity.org/


### AppScan

Web安全扫描


### PC-lint

PC-lint Plus是一种静态分析工具，通过分析C和C ++源代码来发现软件中的缺陷。

### Fortify

https://www.microfocus.com/en-us/cyberres/application-security


### Coverity

Coverity is a fast, accurate, and highly scalable static analysis (SAST) solution that helps development and security teams address security and quality defects in code

https://www.synopsys.com/software-integrity/security-testing/static-analysis-sast.html


### SSA

Static Security Analysis， Intel Vtune Inspector, Intel Parallel Studio XE



## 免费工具

### nmap

网络端口扫描工具

https://nmap.org/


## 抓取工具

### WebScarab

定时抓取数据工具
WebScarab is a framework for analysing applications that communicate using the HTTP and HTTPS protocols.

https://github.com/OWASP/OWASP-WebScarab

