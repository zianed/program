# 结构体对齐 Struct Alignment

现代CPU设计总线为32、64、128位，读数据时只能读坯那一辆为4、8、16的倍数的地址数据，对于32位机器，一个long快约4字节边界存储，那么CPU读两次，效率变低。

When applied to a structure type or variable, sizeof returns the actual size, which may include padding bytes inserted for alignment.

空结构体的占位为1。

## 默认结构体对齐

编译器默认四字节对齐。

tail padding

## 设置结构体对齐

#pragma 设置2字节对齐

```
#pragma pack(push)
#pragma pack(2)
#pragma pack(pop)

```

## 结构体成员偏移计算

实际结构体偏移计算

```
#include <stdio.h>
#include <stdlib.h>

#define offsetof(TYPE, MEMBER) ((size_t) &((TYPE *)0)->MEMBER)

int main(int argc, char **argv)
{
    struct A{
        char p1;
        long p2;
        short p3;
    };
    printf("sizeof(struct A): %u.\n", sizeof(struct A));
    printf("offsetof(struct A, p2): %d.\n", offsetof(struct A, p2));

    struct B{
        char p4;
        short p5;
        long p6;
    };
    printf("sizeof(struct B): %u.\n", sizeof(struct B));
    printf("offsetof(struct B, p5): %d.\n", offsetof(struct B, p5));

    return 0;
}
```

## 参考资料

