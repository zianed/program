# BASE

- 基本可用：Basically Available, 
- 最终一致新：Soft state Eventually consistent.

是CAP中AP的延伸，强调的是可用性。

核心思想：如果不是必须的话，不推荐使用事务或强一致性，鼓励使用可用性和性能优先。



