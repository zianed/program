# 编译

## 导出符号

__dllimport__

__dllexport__


## gcc编译

-o 生成
-c 编译
-g debug
-O 编译优化 O2


- 预编译 gcc -E swap.cpp -o swap.i
- 编译 gcc -S swap.i -o swap.s
- 汇编 as swap.s -o swap.o
- 链接 gcc swap.o -o swap

## gcc与g++

- gcc glibc
- g++ libstdc++


## C编译器

- GCC: GNU C Compiler, GUN Compiler Collection，编译器集合
- MinGW: Minimalist GNU for Windows

环境变量
- LD_LIBRARY_PATH：编译
- PATH:运行
