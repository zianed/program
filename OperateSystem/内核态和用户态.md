# 内核态和用户态

## 用户空间和内核空间的通信方式

- /proc：单向，
- ioctl：单向，
- netlink：双向的，实现全双工、异步、同步通信，基于BSD socket和AF_NETLINK地址簇，使用32位端口，每个netlink协议与一个或一组内核服务/组件相关联。