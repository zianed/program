# 常量

## const变量

变量不允许被重新赋值。

d、*ptrd 不允许被重新赋值，ptrd可以被重新赋值。

```
const doubel d = 0.3;
const doube *ptrd = &d;
```

## const指针

指针不允许被重新赋值，*ptr允许被重新赋值。

```
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
    double d = 0.3;
    double *const ptrd = &d;
    printf("*ptrd: %e\n", *ptrd);
    *ptrd = 0.4;
    printf("*ptrd: %e\n", *ptrd);
    d = 0.5;
    printf("*ptrd: %e\n", *ptrd);

    return 0;
}
```

## const变量const指针

变量、指针都不允许被重新赋值。

```
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
    const double d = 0.3;
    const double *const ptrd = &d;
    printf("*ptrd: %e\n", *ptrd);

    return 0;
}
```

## const成员函数

const成员函数，不修改成员变量，可被const对象调用



