# BlueTooth 蓝牙

Exchange information between short-range radio frequency band.
安全、低功耗、全球通用的短距离无线电通信技术

## 蓝牙标准组织

Bluetooth Special Interest Group(Bluetooth SIG) 蓝牙技术联盟
www.bluetooth.com

建立蓝牙标准，发布蓝牙规范，认证蓝牙互操作，许可蓝牙技术和商标。
遵从Bluetooth Patent/Copyright License Agreement and Bluetooth Trademark License Agreement(BTLA)


## 蓝牙规范

### 规范


Specifications and Test Documents List
https://www.bluetooth.com/specifications/specs/

Core Specification 5.3
https://www.bluetooth.com/specifications/specs/core-specification-5-3/


### 分配号码

Assigned Numbers
https://www.bluetooth.com/specifications/assigned-numbers/

包括了16-bit UUID
The 16-bit UUID Numbers Document contains the following value types: GATT Service, GATT Unit, GATT Declaration, GATT Descriptor, GATT Characteristic and Object Type, 16-bit UUID for members, Protocol Identifier, SDO GATT Service, Service Class and Profile.

16-bit UUID Numbers Document
https://btprodspecificationrefs.blob.core.windows.net/assigned-values/16-bit%20UUID%20Numbers%20Document.pdf


## 蓝牙功能 profile
蓝牙通讯的一系列规范

- A2DP，Advanced Audio Distribution Profile，音频
  
  16bits, 44.1 kHz传输音频信号
  
  List of codecs # Bluetooth
  CodeC是一个硬件或者一个计算机程序，用来编解码数据流或者信号。
  
  * SBC，Low Complexity Subband Coding，
  * aptX，audio process technology，高通发明的音频编解码算法
  * LDAC，高品质音频流，990kbps at 32bit/96kHz，索尼
  * LC3，Low Complexity Communication CodeC，

- BIP，Basic Imaging Profiel，基本图像规范

- GATT，Generic Attribute Profile，通用

- HFP，Hands-Free Profile，车用免手持设备

- HID，Human Interface Device Profile，输入设备



## 蓝牙协议栈 stack




## 蓝牙分类


https://www.bluetooth.com/learn-about-bluetooth/tech-overview/


### BR/EDR(Basic Rate/Enhanced Data Rate) 经典蓝牙


### BLE(Bluetooth Low Energy) 低功耗蓝牙


### MESH
多对多的形式
Bluetooth mesh is a low-power, wireless network that enables many-to-many (m:m) device communication for large-scale device networks.

