# TCP协议


## 目标

- 可靠传输，reliable delivery。
  协议自身机制解决可靠传输 own mechanisms for delivering data reliably.
- 保障数据传输
  - 错误修复码，error-correctint codes, 传输介质可能会丢失或者改变数据，增加错误修复码，方便探测数据错误。
  - 数据重传，ARQ, Automatic Repeat Request, try send again.
    
    ACK，Acknowledgment，确认。
    等待接收确认消息的时间，


## 技术

- 流控，Flow Control
  接受者处理消息处理的太慢，强制发送者减慢发送消息。
  - rate-based, 基于速率
  - window-based, 使用滑动窗口
  
- 拥塞控制，Congestion Control

- 重传超时
  RTT，Round Trip Time，

## 面向连接的，可靠的，byte stream service
  two end，两个端点
  


## 标准
