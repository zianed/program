# extern

## extern "C"

告诉编译器用C的方式来处理库，编译出来的符号是C语言类型的。

```
ifdef __cpplusplus
extern "C" {
#endif

ifdef __cpplusplus
}
#endif
```

## extern与static

- extern是C/C++中表明函数变量全局作用范围的关键字。
  
  extern int a; // 是一个声明。
  
  extern多用于在多个文件之间的跨范围数据传递。
  
- static修饰额全局变量和函数在本模块中使用。

