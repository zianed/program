网络相关书籍


# System

- Computer System：A Programmer's Perspective

CSAPP, 深入理解计算机系统
Author: Randal E. Bryant and David R. O'Hallaron
https://csapp.cs.cmu.edu/
http://csapp.cs.cmu.edu/3e/perspective.html

- Computer Architecture: A Quantitative Approach

计算机体系结构：量化研究方法

- Computer Organization and Design: The Hardware/Software Interface

计算机组成与设计：硬件/软件接口

- Compilers: Principles, Techniques, and Tool

龙书：编译器的原则、技术和工具



# Algorithm

- The Art of Computer Programming

TAOCP, 计算机程序设计艺术
Author: Donald E. Knuth
https://www-cs-faculty.stanford.edu/~knuth/
https://www-cs-faculty.stanford.edu/~knuth/taocp.html

- CLRS
Introduction to Algorithms, 算法导论
Author: Thomas H. Cormen, Charles E. Leiserson, Ronald L. Rivest, and Clifford Stein
https://mitpress.mit.edu/books/introduction-algorithms-third-edition
https://walkccc.me/CLRS/

# OperateSystem

- Operating System Concepts
恐龙书：操作系统概念

Operating System Concepts
https://codex.cs.yale.edu/avi/os-book/

- Modern Operating System
现代操作系统


# Network

Author: W. Richard Stevens
http://www.kohala.com/start/

- APUE: Advanced Proamming in the UNIX Environment, UNIX环境高级编程
- TCPv1: TCP/IP Illustrated, Volume 1: The Protocols
- TCPv2: TCP/IP Illustrated, Volume 2: The Implementation
- TCPv2: TCP/IP Illustrated, Volume 3: TCP for Transactions, HTTP, NNTP, and the UNIX Domain Protocols
- UNIX Network Programming, Volume 1: The Sockets Networking API
- UNIX Network Programming, Volume 2: Interprocess Communications

红宝书：TCP/IP Illustrated，TCP/IP详解


- Computer Networks
