# Serialization  序列化反序列化

## flatbuffers

FlatBuffers is a cross platform serialization library architected for maximum memory efficiency.
It allows you to directly access serialized data without parsing/unpacking it first, while still having great forwards/backwards compatibility.

https://flatbuffers.dev/

## Protocol Buffers

Protocol Buffers are language-neutral, platform-neutral extensible mechanisms for serializing structured data.

https://protobuf.dev/

