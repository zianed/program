# IEEE802

80年2月计划成立的标准组织。

## IEEE802工作组
IEEE 802 LAN/MAN Standards Committee

http://www.ieee802.org/

+ 802.1 Higher Layer LAN Protocols Working Group
+ 802.3 Ethernet Working Group
+ 802.11 Wireless LAN Working Group
+ 802.15 Wireless Personal Area Network (WPAN) Working Group
+ 802.18 Radio Regulatory TAG
+ 802.19 Wireless Coexistence Working Group
+ 802.24 Vertical Applications TAG

最常见的三种：
- 802.3 Ethernet，以太网协议
- 802.11 WLAN，Wi-Fi协议
- 802.15 WPAN，Bluetooth协议


无线网络是利用无线电射频(Radio RF)技术进行通信的网络。分为近距离无线网络和远距离无线网络。

近距离无线网络分为:
- 无线局域网WLAN, Wireless Local Area Network
- 无线个域网WPAN, Wireless Personal Area Network

远距离无线网络分为：
- 蜂窝网络，3G/4G/5G
- 扩频微波通信
- 卫星通信


### 802标准
802标准

GET 802(R) Standards
https://ieeexplore.ieee.org/browse/standards/get-program/page/series?id=68

