 # 链接



## 动态链接器

- 基于命名空间的的动态链接 namespace based dynamic linking，类似于Java类加载器；
- 

动态链接器遍历库搜索路径（Library Search Path，LSPath）来搜索库，由LD_LIBRARY_PATH来设置。
动态链接器用已加载库列表（Already Loaded Library list, ALlist）跟踪加载的库。

## AOSP的Linker

应用程序基于SDK、NDK来构建。为强制应用程序仅使用公共系统库和在Treble架构中管理Frame和Vendor库，Android引入了基于命名空间的动态链接。
/system/bin/linker引入了namespace机制。

Android应用程序以两种方式加载本机库：
1）Java中System.loadLibrary()，调用到NativeLoader。NativeLoader维护从Java类加载器到本地命名空间的映射，命名为classloader-namespce，默认链接到default空间；
2）Native中dlopen或者android_dlopen_ext()；

多个命名空间，将会存在多个库的副本，引起内存消耗。


### 参考资料
- Namespace based Dynamic Linking - Isolating Native Library of Application and System in Android
https://zhenhuaw.me/blog/2017/namespace-based-dynamic-linking.html

- 基于命名空间的动态链接—— 隔离 Android 中应用程序和系统的本地库
 https://zhenhuaw.me/blog/2017/namespace-based-dynamic-linking-chn.html