# 指针

存储的是指向内存分配的地址。

sizeof指针是指针的占用空间。

## 指针初值

- 使用取地址符获取地址；
- 使用new，malloc在堆中进行分配；

## 指针与引用


## 数组指针

指向数组的指针，指向的是一个数组。

```
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
    int ia[3][4] = { 1, 2, 3, 4,
        5, 6, 7, 8,
        9, 10, 11, 12 };
    int (*ip)[4] = ia;
    printf("ip[0][2]: %d\n", ip[0][2]);
    ip = &ia[2];
    printf("ip[0][2]: %d\n", ip[0][2]);

    return 0;
}
```

指针数组，存放指针的数组。普通数组，存放的是指针。

int *intpoint[5];



## 函数指针

指向函数的指针


