# Cross-Device 跨设备操作


### 01. The 4C framework: principles of interaction in digital ecosystems

2014年，从用户、设备以及同时、顺序两个维度来讨论，归结为集体、接续操作；协作、互补操作。

https://people.cs.aau.dk/~jesper/pdf/conferences/Kjeldskov-C81.pdf


### 02. Cross-Device Taxonomy: Survey, Opportunities and Challenges of Interactions Spanning Across Multiple devices

2019年，将跨设备操作进行分类，分类同时使用和顺序使用

https://www.microsoft.com/en-us/research/publication/cross-device-taxonomy-survey-opportunities-and-challenges-of-interactions-spanning-across-multiple-devices/


 