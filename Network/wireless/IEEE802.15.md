# 802.15
802.15协议标准

Wireless Specialty Networks (WSN)
https://www.ieee802.org/15/


- 802.15.1: WPAN/Bluetooth, 是以既有蓝牙标准为基础, 定义了物理层(PHY)和介质访问控制(MAC)规范
- 802.15.2: Coexistence, 主要是为了解决WPAN(802.15)和WLAN(802.11)共存问题
- 802.15.3: High Rate WPAN, 定义了高速率WPAN?
- 802.15.4: Low Rate WPAN, 定义了低速率低功耗WPAN的PHY和MAC规范
- 802.15.5: Mesh Networking, 定义了WPAN设备能够可互操作, 稳定和可扩展的无线网状网络


