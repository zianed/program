# 容器


## 容器更新

- 对于vector, strng, deque, list通过erase返回更新迭代；

```
if () {
    it = list.erase(it);
} else {
    ++it;
}
```

- 对于set, map, multiset, multimap通过后++来更新迭代；

```
if () {
    map.erase(it++);
} else {
    ++it;
}
```

