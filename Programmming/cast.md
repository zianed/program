# cast 转型


- static_cast<type>(expr)  将expr转为type型
- const_cast<type>(expr)  将expr转为const型
- dynamic_cast<type>(expr)  向下转型，转为子类型
- reinterpret_cast<type>(expr)  函数指针类型转换
