# multimedia 多媒体

多媒体主要包含相机、录放音、音视频处理等业务。


## Camera 相机


### Buffer处理

buffer使用分类：

- Framebuffer： 显示Buffer的内存映射；典型的生产者、消费者模式，可以出现一个生产者多个消费者或者多个生产者一个消费者。
- Surface机制： AOSP对应到surface。



### 摄像头

摄像头由PCB板、镜头、固定器、DSP、传感器等部件构成。
- PCB板：摄像头中的硬刷电路板；
- 镜头：将拍摄物在传感器傻瓜成像的器件，通常由几片透镜组成；
- 固定器和滤色片：Holder和Filter；
- DSP数字信号处理芯片：CMSO将DSP集成其中；CCD中将CCD和DSP区分开；
- 传感器：将光信号转换为电信号，CMOS传感器，成像质量相对CCD要低，电量消耗低；CCD传感器，成像质量好，工艺复杂，价格昂贵。传感器尺寸越大，感光性能越好，捕捉的光越多，成像效果越出色。


### 相机参数
- 像素：图片的分辨率越高，代表图片的尺寸越大；
- 光圈：在镜头中控制通过镜头到达传感器的光量多少的装置，控制通光量、控制景深；f/1.4，f/2，f/4，f/8；数值越小，光圈越大，进光量越多，画面比较亮，焦平面越窄，主题背景虚化越大。
- 焦距：从镜头的中心点到传感器平面上所形成的清晰影像之间的距离。焦距越长，能拍到的该问题就越大。
- 


关于3A：
- Auto Focus：自动对焦；
- Auto Exposure：自动曝光；
- Auto White Balance：自动白平衡；




### Camera软件系统分析


| 系统 | 内核 | 简介 | 优势 | 不足 |
| ------- | ------- | ------- | ------- | ------- |
| Android      | Linux   | 手机上通用Camera系统。 | 车上使用exterior view system（EVS）外视系统，支撑后视和环视显示。 | 车机使用独立Auto系统 |
| iOS          |         |         |         |         |
| Windows      |         |         |         |         |
| Tizen        | Linux   |         |         |         |
| QNX          | BlackBerry | 多个生产者多个消费者 | 需要单独购买授权 |         |
| Tesla Vision | Ubuntu  |         |         |         |
| mobileeye    |         | 11路Camera |         |         |
| GHS          | Integrity Multivisor   |         |         |         |


## Audio



## Vedio


