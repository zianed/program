# 802.11
802.11协议标准

Wireless Local Area Networks

http://www.ieee802.org/11

一个标准协议和三个修订：
- IEEE 802.11-2020 - IEEE Standard for Information Technology--Telecommunications and Information Exchange between Systems - Local and Metropolitan Area Networks--Specific Requirements - Part 11: Wireless LAN Medium Access Control (MAC) and Physical Layer (PHY) Specifications

https://standards.ieee.org/standard/802_11-2020.html

https://ieeexplore.ieee.org/document/9363693

- IEEE 802.11ax-2021 - Amendment 1: Enhancements for High-Efficiency WLAN

https://standards.ieee.org/standard/802_11ax-2021.html

https://ieeexplore.ieee.org/document/9442429

- IEEE 802.11ay-2021 - Amendment 2: Enhanced Throughput for Operation in License-exempt Bands above 45 GHz

https://standards.ieee.org/standard/802_11ay-2021.html


- IEEE 802.11ba-2021 - Amendment 3: Wake-Up Radio Operation

https://standards.ieee.org/standard/802_11ba-2021.html




