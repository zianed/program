# display

- LCD: Liquid Crystal, 液晶显示器
- OLED: Organic Light-Emitting Diode, 有机电激光显示


## Fence 机制

AOSP中的同步机制，用于系统中GraphicBuffer的同步，处理CPU、GPU、HWC之间的同步。


## 驱动框架

### FB: frame buffer 机制

### DRM： Direct Render Manager 机制

显卡驱动

分为三层：
- libdrm： 对底层接口进行封装；
- KMS： Kernel Mode Setting，画面更新，设置显示参数；
- GEM： Graphic Execution Manager，负责显示buffer的分配和释放，GPU唯一用到DRM的地方；


## 

### opengl es

用于渲染2D、3D矢量图形的跨语言、跨平台的引用程序编程接口。

https://www.khronos.org/opengles/


OpenGL ES is a royalty-free, cross-platform API for rendering advanced 2D and 3D graphics on embedded and mobile systems - including consoles, phones, appliances and vehicles. It consists of a well-defined subset of desktop OpenGL suitable for low-power devices, and provides a flexible and powerful interface between software and graphics acceleration hardware.

类似的，微软的DirectX，Vulkan


### egl

平台无关的API。EGL是位于OpenGL和本地平台窗口系统（X11或者wayland）之间的一层接口，用于帮助OpenGL绘制的图形显示在窗口系统中。

https://www.khronos.org/egl/

Native Platform Graphics Interface

EGL is an interface between Khronos rendering APIs such as OpenGL ES or OpenVG and the underlying native platform window system. 


### GBM： Generic Buffer Management


EGL -> OpenGL
Mesa GBM -> MESA


## Mesa实现


https://www.mesa3d.org/


